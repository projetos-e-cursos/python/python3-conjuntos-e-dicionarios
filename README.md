# Python3 - Conjuntos e Dicionarios

### Conceitos abordados:
- Referências;
- Conjuntos;
- sets;
- copy();
- extend();
- Utilizar o | para juntar conjuntos;
- Utilizar o & para juntar apenas os números que estão no mesmo conjunto;
- Utilizar o - para remover números repetidos de dois conjuntos;
- ou exclusivo (^);
- add();
- frozenset()
- Dicionário;
- Acessar valor pela chave;
- get();
- keys e values;
- Percorrer dicionário;
- Adicionar e remover elemento do dicionario;
- defaultdict() ;
- Counter();
- most_commom()
