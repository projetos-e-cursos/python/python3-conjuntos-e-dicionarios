

aparicoes = {
    'Erick': 2,
    'Cachorro': 3,
    'eu': 1,
    'andar': 1
}

print(aparicoes['Erick'])

for elemento in aparicoes:
    print(elemento)

for elemento in aparicoes.keys():
    print(elemento)

for elemento in aparicoes.items():
    print(elemento)

for chave, valor in aparicoes.items():
    print(chave, valor)
